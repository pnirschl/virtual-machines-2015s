// Generated from src/main/java/com/gmail/nirschl/peter/vm2015s/parser/SimpleLang.g4 by ANTLR 4.5
package com.gmail.nirschl.peter.vm2015s.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SimpleLangParser}.
 *
 * @param <T>
 *            The return type of the visit operation. Use {@link Void} for
 *            operations with no return type.
 */
public interface SimpleLangVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#program}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitProgram(SimpleLangParser.ProgramContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#funcdef}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitFuncdef(SimpleLangParser.FuncdefContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#pars}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitPars(SimpleLangParser.ParsContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#vardef}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitVardef(SimpleLangParser.VardefContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#type}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitType(SimpleLangParser.TypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#stats}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitStats(SimpleLangParser.StatsContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#stat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitStat(SimpleLangParser.StatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#returnstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitReturnstat(SimpleLangParser.ReturnstatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#branchstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitBranchstat(SimpleLangParser.BranchstatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#loopstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitLoopstat(SimpleLangParser.LoopstatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#vardefstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitVardefstat(SimpleLangParser.VardefstatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#arraydefstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitArraydefstat(SimpleLangParser.ArraydefstatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#assignstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitAssignstat(SimpleLangParser.AssignstatContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#bool}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitBool(SimpleLangParser.BoolContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#bterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitBterm(SimpleLangParser.BtermContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#subbool}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitSubbool(SimpleLangParser.SubboolContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#negation}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitNegation(SimpleLangParser.NegationContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#lesscomp}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitLesscomp(SimpleLangParser.LesscompContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#neqcomp}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitNeqcomp(SimpleLangParser.NeqcompContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#lexpr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitLexpr(SimpleLangParser.LexprContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#expr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitExpr(SimpleLangParser.ExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#subterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitSubterm(SimpleLangParser.SubtermContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#addterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitAddterm(SimpleLangParser.AddtermContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#multerm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitMulterm(SimpleLangParser.MultermContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#term}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitTerm(SimpleLangParser.TermContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#subexpr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitSubexpr(SimpleLangParser.SubexprContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#loadconst}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitLoadconst(SimpleLangParser.LoadconstContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#loadarray}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitLoadarray(SimpleLangParser.LoadarrayContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#loadvar}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitLoadvar(SimpleLangParser.LoadvarContext ctx);

	/**
	 * Visit a parse tree produced by {@link SimpleLangParser#functioncall}.
	 * 
	 * @param ctx
	 *            the parse tree
	 * @return the visitor result
	 */
	T visitFunctioncall(SimpleLangParser.FunctioncallContext ctx);
}