/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

/**
 * A command that can be executed per CLI call.
 * 
 * The setParameters() method must be called before execute().
 * 
 * @author pnirschl
 *
 */
public interface IAppCommand {

	/**
	 * Receives the CLI parameters.
	 * 
	 * @param params
	 *            the encapsulated CLI parameters.
	 */
	public void setParameters(AppParameters params);

	/**
	 * Executes the command.
	 */
	public void execute() throws Exception;

}
