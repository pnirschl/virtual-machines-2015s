// Generated from src/main/java/com/gmail/nirschl/peter/vm2015s/parser/SimpleLang.g4 by ANTLR 4.5
package com.gmail.nirschl.peter.vm2015s.parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SimpleLangParser}.
 */
public interface SimpleLangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#program}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterProgram(SimpleLangParser.ProgramContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#program}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitProgram(SimpleLangParser.ProgramContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#funcdef}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterFuncdef(SimpleLangParser.FuncdefContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#funcdef}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitFuncdef(SimpleLangParser.FuncdefContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#pars}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterPars(SimpleLangParser.ParsContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#pars}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitPars(SimpleLangParser.ParsContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#vardef}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterVardef(SimpleLangParser.VardefContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#vardef}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitVardef(SimpleLangParser.VardefContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#type}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterType(SimpleLangParser.TypeContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#type}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitType(SimpleLangParser.TypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#stats}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterStats(SimpleLangParser.StatsContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#stats}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitStats(SimpleLangParser.StatsContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#stat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterStat(SimpleLangParser.StatContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#stat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitStat(SimpleLangParser.StatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#returnstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterReturnstat(SimpleLangParser.ReturnstatContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#returnstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitReturnstat(SimpleLangParser.ReturnstatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#branchstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterBranchstat(SimpleLangParser.BranchstatContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#branchstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitBranchstat(SimpleLangParser.BranchstatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#loopstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterLoopstat(SimpleLangParser.LoopstatContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#loopstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitLoopstat(SimpleLangParser.LoopstatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#vardefstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterVardefstat(SimpleLangParser.VardefstatContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#vardefstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitVardefstat(SimpleLangParser.VardefstatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#arraydefstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterArraydefstat(SimpleLangParser.ArraydefstatContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#arraydefstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitArraydefstat(SimpleLangParser.ArraydefstatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#assignstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterAssignstat(SimpleLangParser.AssignstatContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#assignstat}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitAssignstat(SimpleLangParser.AssignstatContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#bool}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterBool(SimpleLangParser.BoolContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#bool}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitBool(SimpleLangParser.BoolContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#bterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterBterm(SimpleLangParser.BtermContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#bterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitBterm(SimpleLangParser.BtermContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#subbool}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterSubbool(SimpleLangParser.SubboolContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#subbool}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitSubbool(SimpleLangParser.SubboolContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#negation}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterNegation(SimpleLangParser.NegationContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#negation}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitNegation(SimpleLangParser.NegationContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#lesscomp}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterLesscomp(SimpleLangParser.LesscompContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#lesscomp}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitLesscomp(SimpleLangParser.LesscompContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#neqcomp}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterNeqcomp(SimpleLangParser.NeqcompContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#neqcomp}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitNeqcomp(SimpleLangParser.NeqcompContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#lexpr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterLexpr(SimpleLangParser.LexprContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#lexpr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitLexpr(SimpleLangParser.LexprContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#expr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterExpr(SimpleLangParser.ExprContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#expr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitExpr(SimpleLangParser.ExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#subterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterSubterm(SimpleLangParser.SubtermContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#subterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitSubterm(SimpleLangParser.SubtermContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#addterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterAddterm(SimpleLangParser.AddtermContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#addterm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitAddterm(SimpleLangParser.AddtermContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#multerm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterMulterm(SimpleLangParser.MultermContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#multerm}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitMulterm(SimpleLangParser.MultermContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#term}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterTerm(SimpleLangParser.TermContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#term}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitTerm(SimpleLangParser.TermContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#subexpr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterSubexpr(SimpleLangParser.SubexprContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#subexpr}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitSubexpr(SimpleLangParser.SubexprContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#loadconst}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterLoadconst(SimpleLangParser.LoadconstContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#loadconst}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitLoadconst(SimpleLangParser.LoadconstContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#loadarray}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterLoadarray(SimpleLangParser.LoadarrayContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#loadarray}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitLoadarray(SimpleLangParser.LoadarrayContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#loadvar}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterLoadvar(SimpleLangParser.LoadvarContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#loadvar}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitLoadvar(SimpleLangParser.LoadvarContext ctx);

	/**
	 * Enter a parse tree produced by {@link SimpleLangParser#functioncall}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void enterFunctioncall(SimpleLangParser.FunctioncallContext ctx);

	/**
	 * Exit a parse tree produced by {@link SimpleLangParser#functioncall}.
	 * 
	 * @param ctx
	 *            the parse tree
	 */
	void exitFunctioncall(SimpleLangParser.FunctioncallContext ctx);
}