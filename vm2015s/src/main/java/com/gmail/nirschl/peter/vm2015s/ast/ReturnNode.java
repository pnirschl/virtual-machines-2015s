package com.gmail.nirschl.peter.vm2015s.ast;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * A node indicating a return from a function.
 * 
 * @author pnirschl
 *
 */
public class ReturnNode extends ASTNode {

	public ReturnNode(ParserRuleContext context) {
		super(context);
	}

	@Override
	public void accept(IASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public <T> T accept(ITypedASTVisitor<T> visitor) {
		return visitor.visit(this);
	}
}
