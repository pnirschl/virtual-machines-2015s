/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import org.antlr.v4.runtime.tree.ParseTree;

import com.gmail.nirschl.peter.vm2015s.ast.ArithmeticNode.Operation;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangBaseVisitor;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.AddtermContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.ArraydefstatContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.AssignstatContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.BoolContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.BranchstatContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.BtermContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.ExprContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.FuncdefContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.FunctioncallContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.LesscompContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.LoadarrayContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.LoadconstContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.LoadvarContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.LoopstatContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.MultermContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.NegationContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.NeqcompContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.ProgramContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.ReturnstatContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.StatContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.StatsContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.SubboolContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.SubexprContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.SubtermContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.VardefContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.VardefstatContext;

/**
 * This class builds the abstract syntax tree by visiting the {@link ParseTree}
 * that has been returned by the
 * {@link com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser}.
 * 
 * @author pnirschl
 */
public class SimpleLangASTVisitor extends SimpleLangBaseVisitor<ASTNode> {

	private void negateSubtree(ASTNode n) {
		if (n instanceof ConditionNode) {
			((ConditionNode) n).negate();
		} else if (n instanceof ComparisonNode) {
			((ComparisonNode) n).negate();
		} else {
			return;
		}
		for (ASTNode child : n.getChildNodes()) {
			negateSubtree(child);
		}
	}

	@Override
	public ASTNode visitProgram(ProgramContext ctx) {
		RootNode root = new RootNode(ctx);
		for (FuncdefContext funcdefCtx : ctx.funcdef()) {
			root.getChildNodes().add(visitFuncdef(funcdefCtx));
		}
		return root;
	}

	@Override
	public ASTNode visitFuncdef(FuncdefContext ctx) {
		FunctionNode node = new FunctionNode(ctx);

		// handle function parameters
		SymbolTable symbols = new SymbolTable();
		for (VardefContext vardefCtx : ctx.pars().vardef()) {
			symbols.addVariable(vardefCtx);
		}
		node.setParameters(symbols);

		// handle statements -> descent the tree
		node.getChildNodes().add(visitStats(ctx.stats()));
		return node;
	}

	@Override
	public ASTNode visitStats(StatsContext ctx) {
		StatsNode node = new StatsNode(ctx);
		for (StatContext statCtx : ctx.stat()) {
			node.getChildNodes().add(visitStat(statCtx));
		}
		return node;
	}

	@Override
	public ASTNode visitBool(BoolContext ctx) {
		ConditionNode n = new ConditionNode(ctx);
		for (BtermContext subctx : ctx.bterm()) {
			n.getChildNodes().add(visitBterm(subctx));
		}
		return n;
	}

	@Override
	public ASTNode visitNegation(NegationContext ctx) {
		ASTNode n = visitBterm(ctx.bterm());
		negateSubtree(n);
		return n;
	}

	@Override
	public ASTNode visitLesscomp(LesscompContext ctx) {
		ComparisonNode n = new ComparisonNode(ctx);
		for (ExprContext subctx : ctx.expr()) {
			n.getChildNodes().add(visitExpr(subctx));
		}
		return n;
	}

	@Override
	public ASTNode visitNeqcomp(NeqcompContext ctx) {
		ComparisonNode n = new ComparisonNode(ctx);
		for (ExprContext subctx : ctx.expr()) {
			n.getChildNodes().add(visitExpr(subctx));
		}
		return n;
	}

	@Override
	public ASTNode visitSubbool(SubboolContext ctx) {
		return visitBool(ctx.bool());
	}

	@Override
	public ASTNode visitReturnstat(ReturnstatContext ctx) {
		ReturnNode n = new ReturnNode(ctx);
		n.getChildNodes().add(visitExpr(ctx.expr()));
		return n;
	}

	@Override
	public ASTNode visitBranchstat(BranchstatContext ctx) {
		BranchNode n = new BranchNode(ctx);
		n.setCondition(visitBool(ctx.bool()));
		for (StatsContext subctx : ctx.stats()) {
			n.getChildNodes().add(visitStats(subctx));
		}
		return n;
	}

	@Override
	public ASTNode visitLoopstat(LoopstatContext ctx) {
		WhileLoopNode n = new WhileLoopNode(ctx);
		n.setCondition(visitBool(ctx.bool()));
		n.getChildNodes().add(visitStats(ctx.stats()));
		return n;
	}

	@Override
	public ASTNode visitVardefstat(VardefstatContext ctx) {
		StoreNode n = new StoreNode(ctx);
		n.getChildNodes().add(visitExpr(ctx.expr()));
		return n;
	}

	@Override
	public ASTNode visitArraydefstat(ArraydefstatContext ctx) {
		SimpleArrayInitNode n = new SimpleArrayInitNode(ctx);
		for (ExprContext expr : ctx.expr()) {
			n.getChildNodes().add(visitExpr(expr));
		}
		return n;
	}

	@Override
	public ASTNode visitAssignstat(AssignstatContext ctx) {
		StoreNode n = new StoreNode(ctx);
		for (ExprContext expr : ctx.lexpr().expr()) {
			n.getLExpressions().add(visitExpr(expr));
		}
		n.getChildNodes().add(visitExpr(ctx.expr()));
		return n;
	}

	@Override
	public ASTNode visitLoadconst(LoadconstContext ctx) {
		int intValue;
		String stringValue = ctx.NUM().getText();
		if (stringValue.startsWith("$")) {
			// hex number
			intValue = Integer.parseInt(stringValue.substring(1).toUpperCase(),
					16);
		} else {
			// decimal number
			intValue = Integer.parseInt(stringValue, 10);
		}
		return new ConstantNode(ctx, intValue);
	}

	@Override
	public ASTNode visitLoadarray(LoadarrayContext ctx) {
		LoadNode n = new LoadNode(ctx);
		for (ExprContext e : ctx.expr()) {
			n.getChildNodes().add(visitExpr(e));
		}
		return n;
	}

	@Override
	public ASTNode visitLoadvar(LoadvarContext ctx) {
		return new LoadNode(ctx);
	}

	@Override
	public ASTNode visitFunctioncall(FunctioncallContext ctx) {
		FunctionCallNode n = new FunctionCallNode(ctx);
		for (ExprContext subctx : ctx.expr()) {
			n.getChildNodes().add(visitExpr(subctx));
		}
		return n;
	}

	@Override
	public ASTNode visitSubexpr(SubexprContext ctx) {
		return visitExpr(ctx.expr());
	}

	@Override
	public ASTNode visitSubterm(SubtermContext ctx) {
		if (ctx.subterm() == null) {
			return visitChildren(ctx);
		}

		ArithmeticNode n = new ArithmeticNode(ctx, Operation.SUBTRACTION);
		n.getChildNodes().add(visitSubterm(ctx.subterm()));
		n.getChildNodes().add(visitTerm(ctx.term()));
		return n;
	}

	@Override
	public ASTNode visitAddterm(AddtermContext ctx) {
		if (ctx.addterm() == null) {
			return visitChildren(ctx);
		}

		ArithmeticNode n = new ArithmeticNode(ctx, Operation.ADDITION);
		n.getChildNodes().add(visitAddterm(ctx.addterm()));
		n.getChildNodes().add(visitTerm(ctx.term()));
		return n;
	}

	@Override
	public ASTNode visitMulterm(MultermContext ctx) {
		if (ctx.multerm() == null) {
			return visitChildren(ctx);
		}

		ArithmeticNode n = new ArithmeticNode(ctx, Operation.MULTIPLICATION);
		n.getChildNodes().add(visitMulterm(ctx.multerm()));
		n.getChildNodes().add(visitTerm(ctx.term()));
		return n;
	}

}
