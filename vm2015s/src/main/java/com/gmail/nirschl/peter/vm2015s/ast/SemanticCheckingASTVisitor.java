/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.antlr.v4.runtime.Token;

/**
 * Checks the semantic validity of the abstract syntax tree
 * {@link com.gmail.nirschl.peter.vm2015s.ast.AbstractSyntaxTree}.
 * 
 * @author pnirschl
 *
 */
public class SemanticCheckingASTVisitor implements IASTVisitor {

	public static final String UNDECLARED_VARIABLE_USAGE = "Undeclared variable";
	public static final String TYPE_MISMATCH = "Type mismatch";
	public static final String INVALID_FUNCTION_CALL = "Function call";

	public static class SemanticError {
		private final Token sourcePosition;
		private String title;
		private String message;

		public SemanticError(Token sourcePosition) {
			this.sourcePosition = sourcePosition;
		}

		public SemanticError(Token sourcePosition, String title, String message) {
			this(sourcePosition);
			this.title = title;
			this.message = message;
		}

		public SemanticError constructTypeMismatch(int expectedType,
				int actualType) {
			this.title = TYPE_MISMATCH;
			this.message = String.format("expected type %s but got type %s",
					TypeInformation.typeToString(expectedType),
					TypeInformation.typeToString(actualType));
			return this;
		}

		public SemanticError constructUndeclaredVariableUsage(final String id) {
			this.title = UNDECLARED_VARIABLE_USAGE;
			this.message = "use of undeclared variable: " + id;
			return this;
		}

		public SemanticError constructUndeclaredFunctionUsage(final String id) {
			this.title = INVALID_FUNCTION_CALL;
			this.message = "call of undefined function: " + id;
			return this;
		}

		@Override
		public String toString() {
			return String.format("%s at %d:%d - %s", title,
					sourcePosition.getLine(),
					sourcePosition.getCharPositionInLine(), message);
		}
	}

	private final TypeDetectingASTVisitor typeDetector = new TypeDetectingASTVisitor();
	private final List<SemanticError> errors = new ArrayList<SemanticError>();
	private final LinkedList<SymbolTable> symbolTableStack = new LinkedList<SymbolTable>();
	private final List<String> declaredFunctions = new ArrayList<String>();

	public boolean isSemanticallyCorrect() {
		return errors.isEmpty();
	}

	public List<SemanticError> getErrorList() {
		return errors;
	}

	private void visitChildren(ASTNode node) {
		if (node.getChildNodes() == null) {
			return;
		}
		for (ASTNode child : node.getChildNodes()) {
			child.accept(this);
		}
	}

	private void assertChildrenHavetype(ASTNode n, int type) {
		for (ASTNode child : n.getChildNodes()) {
			int typeOfChild = child.accept(typeDetector);
			if (typeOfChild != type) {
				errors.add(new SemanticError(child.getStartPosition())
						.constructTypeMismatch(type, typeOfChild));
			}
		}
	}

	public void visit(RootNode n) {
		visitChildren(n);
	}

	public void visit(FunctionNode n) {
		symbolTableStack.push(n.getParameters().duplicate());
		declaredFunctions.add(n.getId());
		visitChildren(n);
		symbolTableStack.clear();
	}

	public void visit(StatsNode n) {
		symbolTableStack.push(symbolTableStack.peek().duplicate());
		visitChildren(n);
		symbolTableStack.pop();
	}

	public void visit(UnsupportedNode n) {
		// nothing to do
	}

	public void visit(ArithmeticNode n) {
		visitChildren(n);
		assertChildrenHavetype(n, TypeInformation.INT);
	}

	public void visit(ConstantNode n) {
		// nothing to do
	}

	public void visit(ReturnNode n) {
		visitChildren(n);
		assertChildrenHavetype(n, TypeInformation.INT);
	}

	public void visit(StoreNode n) {
		visitChildren(n);
		try {
			int expectedType = TypeInformation.NO_TYPE;
			int actualType = n.accept(typeDetector);
			if (n.isDefinition()) {
				symbolTableStack.peek().addVariable(n.getId(),
						n.getDeclarationType());
				expectedType = n.getDeclarationType();
			} else {
				expectedType = symbolTableStack.peek().getVariableType(
						n.getId())
						- n.getLExpressions().size();
				n.setDeclarationType(expectedType);
			}
			if (actualType != expectedType) {
				errors.add(new SemanticError(n.getStartPosition())
						.constructTypeMismatch(expectedType, actualType));
			}
		} catch (UndefinedVariableUsageException e) {
			errors.add(new SemanticError(n.getStartPosition())
					.constructUndeclaredVariableUsage(n.getId()));
		}
	}

	public void visit(LoadNode n) {
		visitChildren(n);
		try {
			int actualType = symbolTableStack.peek().getVariableType(n.getId());
			n.setDeclarationType(actualType);
		} catch (UndefinedVariableUsageException e) {
			errors.add(new SemanticError(n.getStartPosition())
					.constructUndeclaredVariableUsage(n.getId()));
		}
	}

	public void visit(ConditionNode n) {
		// boolean operations are inherently correct because of the grammar
		visitChildren(n);
	}

	public void visit(ComparisonNode n) {
		// boolean operations are inherently correct because of the grammar
		visitChildren(n);
	}

	public void visit(BranchNode n) {
		// boolean operations are inherently correct because of the grammar
		visitChildren(n);
	}

	public void visit(WhileLoopNode n) {
		// boolean operations are inherently correct because of the grammar
		visitChildren(n);
	}

	public void visit(FunctionCallNode n) {
		if (!declaredFunctions.contains(n.getId())) {
			errors.add(new SemanticError(n.getStartPosition())
					.constructUndeclaredFunctionUsage(n.getId()));
		}
		// TODO type checking for function calls
		visitChildren(n);
	}

	public void visit(SimpleArrayInitNode n) {
		// the grammar asserts that only "array of int" can be created
		symbolTableStack.peek().addVariable(n.getId(),
				TypeInformation.INT_ARRAY);

		// assert that child node (expressions) are of type INT
		for (ASTNode child : n.getChildNodes()) {
			int actualType = child.accept(typeDetector);
			if (actualType != TypeInformation.INT) {
				errors.add(new SemanticError(child.getStartPosition())
						.constructTypeMismatch(TypeInformation.INT, actualType));
			}
		}
		visitChildren(n);
	}
}
