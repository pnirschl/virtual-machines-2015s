/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.ArraydefstatContext;

public class SimpleArrayInitNode extends ASTNode {

	private final String id;
	private final int arraySize;

	public SimpleArrayInitNode(ArraydefstatContext ctx) {
		super(ctx);
		id = ctx.vardef().ID().getText();
		arraySize = ctx.expr().size();
	}

	@Override
	public void accept(IASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public <T> T accept(ITypedASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

	/**
	 * Get the identifier of the array to be initialized.
	 * 
	 * @return the ID of the array to be initialized.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Get the number of elements that will be stored in the array.
	 * 
	 * @return the number of elements to be stored in the array as integer.
	 */
	public int getArraySize() {
		return arraySize;
	}
}
