/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import com.gmail.nirschl.peter.vm2015s.ast.ASTNode;

/**
 * Prints the generated
 * {@link com.gmail.nirschl.peter.vm2015s.ast.AbstractSyntaxTree} to the
 * standard output.
 * 
 * @author pnirschl
 *
 */
public class ASTPrinter implements IAppCommand {

	AppParameters params = null;

	public void setParameters(AppParameters params) {
		this.params = params;
	}

	public void execute() throws Exception {
		System.out.println("----- Abstract Syntax Tree -----");
		if (params.getAST() != null) {
			printAstNode(params.getAST().getRoot(), "", true);
		}
		System.out.println();
	}

	/**
	 * Print the AST to the standard output recursively.
	 * 
	 * @param node
	 * @param prefix
	 * @param isTail
	 */
	public void printAstNode(ASTNode node, String prefix, boolean isTail) {
		// Inspired by
		// http://stackoverflow.com/questions/4965335/how-to-print-binary-tree-diagram
		System.out.println(prefix + (isTail ? "└── " : "├── ")
				+ node.getClass().getSimpleName());
		if (node.getChildNodes() != null) {
			for (int i = 0; i < node.getChildNodes().size() - 1; i++) {
				printAstNode(node.getChildNodes().get(i), prefix
						+ (isTail ? "    " : "│   "), false);
			}
			if (node.getChildNodes().size() > 0) {
				printAstNode(
						node.getChildNodes().get(
								node.getChildNodes().size() - 1), prefix
								+ (isTail ? "    " : "│   "), true);
			}
		}
	}
}
