/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

/**
 * Represents a single node within the {@link AbstractSyntaxTree}. It holds all
 * the semantic information that are relevant to generate the byte-code for the
 * given node (as well as its sub-nodes).
 * 
 * @author pnirschl
 *
 */
public abstract class ASTNode {

	private final ParserRuleContext context;
	private final ArrayList<ASTNode> children = new ArrayList<ASTNode>();

	public ASTNode(ParserRuleContext context) {
		this.context = context;
	}

	/**
	 * Return all child nodes of the node.
	 * 
	 * @return a {@link java.lang.List} of all sub-nodes
	 */
	public List<ASTNode> getChildNodes() {
		return children;
	}

	/**
	 * Accept an AST visitor (
	 * {@link com.gmail.nirschl.peter.vm2015s.ast.IASTVisitor}) by calling the
	 * visit() operation based on the type of the node.
	 * 
	 * @param visitor
	 */
	public abstract void accept(IASTVisitor visitor);

	/**
	 * Accept a typed AST visitor (
	 * {@link com.gmail.nirschl.peter.vm2015s.ast.ITypedASTVisitor}) by calling
	 * the visit() operation based on the type of the node.
	 * 
	 * @param <T>
	 *            type parameter
	 * 
	 * @param visitor
	 * @return some value depending on the visitor
	 */
	public abstract <T> T accept(ITypedASTVisitor<T> visitor);

	/**
	 * Get the source of the node.
	 * 
	 * @return a {@link org.antlr.v4.runtime.Token} holding the position within
	 *         the source code.
	 */
	public Token getStartPosition() {
		return context.getStart();
	}
}
