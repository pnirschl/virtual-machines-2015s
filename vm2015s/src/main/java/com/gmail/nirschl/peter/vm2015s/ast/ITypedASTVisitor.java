/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

/**
 * A visitor that operates on the elements of the abstract syntax tree (
 * {@link com.gmail.nirschl.peter.vm2015s.ast.AbstractSyntaxTree}) returning
 * nodes of a given type.
 * 
 * @author pnirschl
 *
 */
public interface ITypedASTVisitor<T> {
	public T visit(RootNode n);

	public T visit(FunctionNode n);

	public T visit(StatsNode n);

	public T visit(UnsupportedNode n);

	public T visit(ArithmeticNode n);

	public T visit(ConstantNode n);

	public T visit(ReturnNode n);

	public T visit(StoreNode n);

	public T visit(LoadNode n);

	public T visit(ConditionNode n);

	public T visit(ComparisonNode n);

	public T visit(BranchNode n);

	public T visit(WhileLoopNode n);

	public T visit(FunctionCallNode n);

	public T visit(SimpleArrayInitNode n);
}
