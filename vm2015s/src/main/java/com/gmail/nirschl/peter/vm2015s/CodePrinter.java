/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;

import com.gmail.nirschl.peter.vm2015s.codegen.CodeGeneratingASTVisitor;

public class CodePrinter implements IAppCommand {

	private AppParameters parameters;

	public void setParameters(AppParameters params) {
		this.parameters = params;
	}

	public void execute() throws Exception {
		// generate the byte-code
		CodeGeneratingASTVisitor visitor = new CodeGeneratingASTVisitor(
				parameters);
		parameters.getAST().getRoot().accept(visitor);

		// write byte-code to stdout
		System.out.println("----- Generated Bytecode -----");
		JavaClass generatedClass = visitor.getGeneratedClass();
		System.out.println(generatedClass.getConstantPool().toString());
		for (Method method : generatedClass.getMethods()) {
			System.out.println("[Method] " + method.toString());
			System.out.println(method.getCode().toString(true));
			System.out.println();
		}
		System.out.println();
	}

}
