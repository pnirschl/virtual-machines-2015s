/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.codegen;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.bcel.Constants;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.AALOAD;
import org.apache.bcel.generic.ALOAD;
import org.apache.bcel.generic.ASTORE;
import org.apache.bcel.generic.BasicType;
import org.apache.bcel.generic.BranchHandle;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.DUP;
import org.apache.bcel.generic.GOTO;
import org.apache.bcel.generic.IADD;
import org.apache.bcel.generic.IALOAD;
import org.apache.bcel.generic.IASTORE;
import org.apache.bcel.generic.IF_ICMPEQ;
import org.apache.bcel.generic.IF_ICMPGE;
import org.apache.bcel.generic.IF_ICMPLT;
import org.apache.bcel.generic.IF_ICMPNE;
import org.apache.bcel.generic.ILOAD;
import org.apache.bcel.generic.IMUL;
import org.apache.bcel.generic.INVOKESTATIC;
import org.apache.bcel.generic.IRETURN;
import org.apache.bcel.generic.ISTORE;
import org.apache.bcel.generic.ISUB;
import org.apache.bcel.generic.InstructionHandle;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.LDC;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.NEWARRAY;
import org.apache.bcel.generic.NOP;
import org.apache.bcel.generic.SIPUSH;
import org.apache.bcel.generic.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gmail.nirschl.peter.vm2015s.AppParameters;
import com.gmail.nirschl.peter.vm2015s.ast.ASTNode;
import com.gmail.nirschl.peter.vm2015s.ast.ArithmeticNode;
import com.gmail.nirschl.peter.vm2015s.ast.BranchNode;
import com.gmail.nirschl.peter.vm2015s.ast.ComparisonNode;
import com.gmail.nirschl.peter.vm2015s.ast.ConditionNode;
import com.gmail.nirschl.peter.vm2015s.ast.ConditionNode.Operation;
import com.gmail.nirschl.peter.vm2015s.ast.ConstantNode;
import com.gmail.nirschl.peter.vm2015s.ast.FunctionCallNode;
import com.gmail.nirschl.peter.vm2015s.ast.FunctionNode;
import com.gmail.nirschl.peter.vm2015s.ast.IASTVisitor;
import com.gmail.nirschl.peter.vm2015s.ast.LoadNode;
import com.gmail.nirschl.peter.vm2015s.ast.ReturnNode;
import com.gmail.nirschl.peter.vm2015s.ast.RootNode;
import com.gmail.nirschl.peter.vm2015s.ast.SimpleArrayInitNode;
import com.gmail.nirschl.peter.vm2015s.ast.StatsNode;
import com.gmail.nirschl.peter.vm2015s.ast.StoreNode;
import com.gmail.nirschl.peter.vm2015s.ast.SymbolTable;
import com.gmail.nirschl.peter.vm2015s.ast.TypeInformation;
import com.gmail.nirschl.peter.vm2015s.ast.UndefinedVariableUsageException;
import com.gmail.nirschl.peter.vm2015s.ast.UnsupportedNode;
import com.gmail.nirschl.peter.vm2015s.ast.WhileLoopNode;

/**
 * Generates Java byte-code by visiting the abstract syntax tree
 * {@link com.gmail.nirschl.peter.vm2015s.ast.AbstractSyntaxTree}.
 * 
 * @author pnirschl
 *
 */
public class CodeGeneratingASTVisitor implements IASTVisitor {

	/**
	 * Defines which boolean statement is being processed at the moment.
	 */
	public static enum BooleanStatMode {
		IF, WHILE
	}

	private static final Logger LOG = LoggerFactory
			.getLogger(CodeGeneratingASTVisitor.class);

	private LinkedList<SymbolTable> symbolTableStack = new LinkedList<SymbolTable>();
	private ArrayList<MethodGen> methodgens = new ArrayList<MethodGen>();
	private ClassGen classgen;
	private MethodGen methodgen;
	private InstructionList instructions;

	private void visitChildren(ASTNode n) {
		if (n.getChildNodes() == null) {
			return;
		}

		for (ASTNode child : n.getChildNodes()) {
			child.accept(this);
		}
	}

	private Type[] getFunctionArgumentTypes(FunctionNode n) {
		Integer[] internalTypes = n.getParameters().getTypes();
		Type[] bcelTypes = new Type[internalTypes.length];

		for (int i = 0; i < internalTypes.length; i++) {
			if (internalTypes[i] == TypeInformation.INT) {
				bcelTypes[i] = Type.INT;
			} else {
				// construct Java signature for the array type
				StringBuilder sb = new StringBuilder();
				for (int j = 0; j < internalTypes[i]; j++) {
					sb.append("[");
				}
				sb.append("I;");
				bcelTypes[i] = Type.getType(sb.toString());
			}
		}
		return bcelTypes;
	}

	public CodeGeneratingASTVisitor(AppParameters parameters) {
		classgen = new ClassGen(parameters.getClassName(), "java/lang/Object",
				null, Constants.ACC_PUBLIC, new String[] {});
	}

	public JavaClass getGeneratedClass() {
		return classgen.getJavaClass();
	}

	public void visit(RootNode n) {
		visitChildren(n);
	}

	public void visit(FunctionNode n) {
		symbolTableStack.add(n.getParameters().duplicate());

		int accessFlags = Constants.ACC_PUBLIC | Constants.ACC_STATIC;
		Type[] argTypes = getFunctionArgumentTypes(n);
		String[] argNames = n.getParameters().getIds();

		instructions = new InstructionList();
		methodgen = new MethodGen(accessFlags, Type.INT, argTypes, argNames,
				n.getId(), classgen.getClassName(), instructions,
				classgen.getConstantPool());
		visitChildren(n);
		methodgen.stripAttributes(true);
		methodgen.update();
		methodgen.setMaxStack();
		methodgen.setMaxLocals();
		methodgen.update();
		classgen.addMethod(methodgen.getMethod());
		methodgens.add(methodgen);
		symbolTableStack.clear();
	}

	public void visit(StatsNode n) {
		symbolTableStack.push(symbolTableStack.peek().duplicate());
		visitChildren(n);
		symbolTableStack.pop();
	}

	public void visit(UnsupportedNode n) {
		// For testing and debugging it is useful to create some code.
		// The UnsupportedNode will be resolved to a NOP instruction.
		instructions.append(new NOP());
	}

	public void visit(ArithmeticNode n) {
		// put the operands on the stack by processing the children of n
		visitChildren(n);
		// add the corresponding arithmetic command
		switch (n.getOperation()) {
		case ADDITION:
			instructions.append(new IADD());
			break;

		case SUBTRACTION:
			instructions.append(new ISUB());
			break;

		case MULTIPLICATION:
			instructions.append(new IMUL());
			break;

		default:
			LOG.warn("Unsupported arithmetic operation detected!");
			instructions.append(new NOP());
			break;
		}
	}

	public void visit(ConstantNode n) {
		int poolindex = classgen.getConstantPool().addInteger(n.getValue());
		instructions.append(new LDC(poolindex));
	}

	public void visit(ReturnNode n) {
		visitChildren(n);
		instructions.append(new IRETURN());
	}

	public void visit(StoreNode n) {
		if (n.isDefinition()) {
			symbolTableStack.peek().addVariable(n.getId(),
					n.getDeclarationType());
		}

		try {
			if (symbolTableStack.peek().getVariableType(n.getId()) == TypeInformation.INT) {
				visitChildren(n);
				try {
					// store to regular variable
					instructions.append(new ISTORE(symbolTableStack.peek()
							.getLocalNumber(n.getId())));
				} catch (UndefinedVariableUsageException e) {
					throw new RuntimeException(e);
				}
			} else {
				// array store (dereference first)
				for (int i = 0; i < n.getLExpressions().size(); i++) {
					if (i == 0) {
						instructions.append(new ALOAD(symbolTableStack.peek()
								.getLocalNumber(n.getId())));
					} else {
						instructions.append(new AALOAD());
					}
					n.getLExpressions().get(i).accept(this);
				}
				// push value to the stack
				visitChildren(n);
				instructions.append(new IASTORE());
			}
		} catch (UndefinedVariableUsageException e) {
			throw new RuntimeException(e);
		}
	}

	public void visit(LoadNode n) {
		try {
			if (symbolTableStack.peek().getVariableType(n.getId()) == TypeInformation.INT) {
				// load regular variable
				instructions.append(new ILOAD(symbolTableStack.peek()
						.getLocalNumber(n.getId())));
			} else {
				if (n.getChildNodes().size() > 0) {
					// array load (dereference first)
					for (int i = 0; i < n.getChildNodes().size(); i++) {
						if (i == 0) {
							instructions.append(new ALOAD(symbolTableStack
									.peek().getLocalNumber(n.getId())));
						} else {
							instructions.append(new AALOAD());
						}
						n.getChildNodes().get(i).accept(this);
					}
					instructions.append(new IALOAD());
				} else {
					// if there are no expressions simply place the array
					// reference on the stack
					instructions.append(new ALOAD(symbolTableStack.peek()
							.getLocalNumber(n.getId())));
				}
			}
		} catch (UndefinedVariableUsageException e) {
			throw new RuntimeException(e);
		}
	}

	private BooleanStatMode booleanStatMode;
	private ConditionNode.Operation booleanOperation;
	private List<BranchHandle> boolJumpTargetIf = new ArrayList<BranchHandle>();
	private List<BranchHandle> boolJumpTargetElse = new ArrayList<BranchHandle>();
	private List<BranchHandle> boolJumpTargetStart = new ArrayList<BranchHandle>();
	private List<BranchHandle> boolJumpTargetEnd = new ArrayList<BranchHandle>();

	public void visit(ConditionNode n) {
		List<BranchHandle> target;

		booleanOperation = n.getOperation();
		visitChildren(n);

		// select target
		if (booleanOperation == Operation.OR) {
			switch (booleanStatMode) {
			case IF:
				target = boolJumpTargetElse;
				break;
			case WHILE:
				target = boolJumpTargetEnd;
				break;
			default:
				target = null;
				break;
			}

			target.add(instructions.append(new GOTO(null)));
		}
	}

	public void visit(ComparisonNode n) {
		List<BranchHandle> target;

		visitChildren(n);

		// decide jump target
		if (booleanStatMode == BooleanStatMode.IF) {
			switch (booleanOperation) {
			case AND:
				target = boolJumpTargetElse;
				break;
			case OR:
				target = boolJumpTargetIf;
				break;
			default:
				target = null;
				break;
			}
		} else {
			switch (booleanOperation) {
			case AND:
				target = boolJumpTargetEnd;
				break;
			case OR:
				target = boolJumpTargetStart;
				break;
			default:
				target = null;
				break;
			}
		}

		if (booleanOperation == Operation.AND) {
			n.negate();
		}

		// decide operation to generate
		switch (n.getOperation()) {
		case EQUAL:
			target.add(instructions.append(new IF_ICMPEQ(null)));
			break;
		case GREATER_OR_EQUAL:
			target.add(instructions.append(new IF_ICMPGE(null)));
			break;
		case LESS:
			target.add(instructions.append(new IF_ICMPLT(null)));
			break;
		case NOT_EQUAL:
			target.add(instructions.append(new IF_ICMPNE(null)));
			break;
		default:
			break;
		}

		if (booleanOperation == Operation.AND) {
			n.negate();
		}
	}

	public void visit(BranchNode n) {
		InstructionHandle ifStart = null;
		InstructionHandle elseStart = null;

		booleanStatMode = BooleanStatMode.IF;
		n.getCondition().accept(this);
		// save the current instruction list
		InstructionList currentList = this.instructions;

		// generate if-block
		InstructionList ifBlock = new InstructionList();
		this.instructions = ifBlock;
		n.getChildNodes().get(0).accept(this);
		BranchHandle gotoEnd = null;
		if (n.getChildNodes().size() > 1) {
			// if there is an else-block we have to skip it (i.e. jump to end)
			gotoEnd = instructions.append(new GOTO(null));
		}
		ifStart = currentList.append(instructions);

		// generate the else-block (if any)
		InstructionList elseBlock = null;
		if (n.getChildNodes().size() > 1) {
			elseBlock = new InstructionList();
			this.instructions = elseBlock;
			n.getChildNodes().get(1).accept(this);
			elseStart = currentList.append(instructions);
		}

		// generate an NOP as jump target beyond the if block
		InstructionHandle endStart = currentList.append(new NOP());

		// update jump targets
		if (elseBlock == null) {
			elseStart = endStart;
		}

		for (BranchHandle branchHandle : boolJumpTargetIf) {
			branchHandle.setTarget(ifStart);
		}
		for (BranchHandle branchHandle : boolJumpTargetElse) {
			branchHandle.setTarget(elseStart);
		}
		if (gotoEnd != null) {
			gotoEnd.setTarget(endStart);
		}

		boolJumpTargetElse.clear();
		boolJumpTargetIf.clear();

		// restore instruction list
		instructions = currentList;
		instructions.update();
	}

	public void visit(WhileLoopNode n) {
		InstructionList currentInstructions = this.instructions;
		InstructionHandle conditionStart;
		InstructionHandle bodyStart;
		InstructionHandle end;

		booleanStatMode = BooleanStatMode.WHILE;

		// generate condition
		InstructionList conditionInstructions = new InstructionList();
		this.instructions = conditionInstructions;
		n.getCondition().accept(this);
		conditionStart = currentInstructions.append(conditionInstructions);

		// generate loop-body
		InstructionList bodyInstructions = new InstructionList();
		this.instructions = bodyInstructions;
		n.getChildNodes().get(0).accept(this);
		bodyStart = currentInstructions.append(bodyInstructions);
		currentInstructions.append(new GOTO(conditionStart));
		end = currentInstructions.append(new NOP());

		// update jump targets
		for (BranchHandle b : boolJumpTargetStart) {
			b.setTarget(bodyStart);
		}
		for (BranchHandle b : boolJumpTargetEnd) {
			b.setTarget(end);
		}

		boolJumpTargetStart.clear();
		boolJumpTargetEnd.clear();

		// restore instruction list
		this.instructions = currentInstructions;
		this.instructions.update();
	}

	public void visit(FunctionCallNode n) {
		visitChildren(n);
		int index = classgen.getConstantPool().addMethodref(
				classgen.getClassName(), n.getId(),
				getMethodSignature(n.getId()));
		instructions.append(new INVOKESTATIC(index));
	}

	private String getMethodSignature(String name) {
		for (Method m : classgen.getMethods()) {
			if (m.getName().equals(name)) {
				return m.getSignature();
			}
		}
		return "";
	}

	public void visit(SimpleArrayInitNode n) {
		symbolTableStack.peek().addVariable(n.getId(),
				TypeInformation.INT_ARRAY);

		// create new array
		instructions.append(new SIPUSH((short) n.getArraySize()));
		instructions.append(new NEWARRAY(BasicType.INT));

		// initialize the array
		for (short i = 0; i < n.getChildNodes().size(); i++) {
			instructions.append(new DUP());
			instructions.append(new SIPUSH(i));
			n.getChildNodes().get(i).accept(this);
			instructions.append(new IASTORE());
		}

		// store ref to initialized array
		try {
			int variable_nr = symbolTableStack.peek().getLocalNumber(n.getId());
			instructions.append(new ASTORE(variable_nr));
		} catch (UndefinedVariableUsageException e) {
			throw new RuntimeException(e);
		}
	}
}
