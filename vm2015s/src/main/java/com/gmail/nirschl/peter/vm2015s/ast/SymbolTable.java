/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import java.util.ArrayList;
import java.util.ListIterator;

import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.VardefContext;

/**
 * The symbol table holds the type information of the defined variables. Uses of
 * undefined variables are identified by invalid symbol table access calls.
 * 
 * @author pnirschl
 */
public class SymbolTable {

	private static class TableEntry {
		private String id;
		private Integer type;

		public TableEntry(String id, Integer type) {
			this.id = id;
			this.type = type;
		}

		public String getId() {
			return id;
		}

		public Integer getType() {
			return type;
		}
	}

	private ArrayList<TableEntry> symbols = new ArrayList<TableEntry>();

	/**
	 * Add variable to the symbol table.
	 * 
	 * @param ctx
	 *            The {@link VardefContext} input from the parser
	 */
	public void addVariable(VardefContext ctx) {
		// NOTE ctx.type() resembles the rule:
		//
		// type: ( 'array of' )* 'int' ;
		//
		// so it contains at least one 'int'. the rest of the symbols are array
		// derefenece operations. Therefore the type can be deduced simply by
		// counting the children of ctx.type().
		addVariable(ctx.ID().getText(), ctx.type().children.size() - 1);
	}

	/**
	 * Add variable to the symbol table.
	 * 
	 * @param id
	 *            The identifier of the variable
	 * @param type
	 *            The type (i.e. array-depth) of the variable
	 */
	public void addVariable(String id, Integer type) {
		symbols.add(new TableEntry(id, type));
	}

	/**
	 * Returns the type of the given identifier
	 * 
	 * @param id
	 *            identifier of the variable
	 * @return the type (i.e. array-depth) of the variable
	 * @throws UndefinedVariableUsageException
	 *             if the identifier can not be found in the symbol table
	 */
	public int getVariableType(String id)
			throws UndefinedVariableUsageException {

		return getLastOccurenceOfId(id).getType();
	}

	/**
	 * This method is used for code generation. It returns the local variable
	 * number.
	 * 
	 * @param id
	 *            identifier of the variable
	 * @return the local variable number for Java byte-code generation
	 * @throws UndefinedVariableUsageException
	 *             if the identifier can not be found in the symbol table
	 */
	public int getLocalNumber(String id) throws UndefinedVariableUsageException {

		for (int i = symbols.size() - 1; i >= 0; i--) {
			if (symbols.get(i).getId().equals(id)) {
				return i;
			}
		}
		throw new UndefinedVariableUsageException();
	}

	/**
	 * Get the number of symbols that are stored in the symbol table.
	 * 
	 * @return the numer of symbols as Integer
	 */
	public int getSymbolCount() {
		return symbols.size();
	}

	/**
	 * Return the last element within the symbol table, that matches the given
	 * identifier. This is the most recently added element.
	 * 
	 * @param id
	 *            identifier of the variable
	 * @return the most recently added element of the symbol table that has a
	 *         matching identifier
	 * @throws UndefinedVariableUsageException
	 *             if the identifier can not be found in the symbol table
	 */
	private TableEntry getLastOccurenceOfId(String id)
			throws UndefinedVariableUsageException {
		ListIterator<TableEntry> iterator = symbols
				.listIterator(symbols.size());

		while (iterator.hasPrevious()) {
			TableEntry e = iterator.previous();
			if (e.getId().equals(id)) {
				return e;
			}
		}
		throw new UndefinedVariableUsageException();
	}

	/**
	 * Clone the symbol table and return a new reference.
	 * 
	 * @return a deep-copy of the symbol table
	 */
	public SymbolTable duplicate() {
		SymbolTable newTable = new SymbolTable();
		newTable.symbols.addAll(this.symbols);
		return newTable;
	}

	/**
	 * Get an array containing all the IDs that are stored in the symbol table.
	 * 
	 * @return a String array of all IDs in the table.
	 */
	public String[] getIds() {
		ArrayList<String> list = new ArrayList<String>();
		for (TableEntry e : symbols) {
			list.add(e.getId());
		}
		return list.toArray(new String[] {});
	}

	/**
	 * Get an array containing all the types that are stored in the symbol
	 * table.
	 * 
	 * @return an Integer array of all types in the table.
	 */
	public Integer[] getTypes() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (TableEntry e : symbols) {
			list.add(e.getType());
		}
		return list.toArray(new Integer[] {});
	}
}
