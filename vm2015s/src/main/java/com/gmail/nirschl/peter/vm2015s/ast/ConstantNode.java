package com.gmail.nirschl.peter.vm2015s.ast;

import org.antlr.v4.runtime.ParserRuleContext;

/**
 * A node that represents a constant, i.e. an numeric literal that is being
 * interpreted as integer.
 * 
 * @author pnirschl
 *
 */
public class ConstantNode extends ASTNode {

	private final int value;

	public ConstantNode(ParserRuleContext context, final int value) {
		super(context);
		this.value = value;
	}

	@Override
	public void accept(IASTVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public <T> T accept(ITypedASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public int getValue() {
		return value;
	}
}
