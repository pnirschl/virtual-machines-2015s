/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.LesscompContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.NeqcompContext;

/**
 * AST node representing a comparison of two integer values (i.e. its two
 * children).
 * 
 * @author pnirschl
 *
 */
public class ComparisonNode extends ASTNode {

	public static enum Operation {
		LESS, GREATER_OR_EQUAL, NOT_EQUAL, EQUAL
	}

	private Operation opcode;

	public ComparisonNode(LesscompContext ctx) {
		super(ctx);
		this.opcode = Operation.LESS;
	}

	public ComparisonNode(NeqcompContext ctx) {
		super(ctx);
		this.opcode = Operation.NOT_EQUAL;
	}

	/**
	 * Negate the operation.
	 */
	public void negate() {
		switch (opcode) {
		case LESS:
			opcode = Operation.GREATER_OR_EQUAL;
			break;
		case GREATER_OR_EQUAL:
			opcode = Operation.LESS;
			break;
		case EQUAL:
			opcode = Operation.NOT_EQUAL;
			break;
		case NOT_EQUAL:
			opcode = Operation.EQUAL;
			break;
		default:
			// do nothing
			break;
		}
	}

	@Override
	public void accept(IASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public <T> T accept(ITypedASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public Operation getOperation() {
		return opcode;
	}

}
