/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.BoolContext;

/**
 * AST node representing a condition. Its children are logically connected by
 * either the logical OR or the logical AND operation.
 * 
 * @author pnirschl
 *
 */
public class ConditionNode extends ASTNode {

	public static enum Operation {
		AND, OR
	}

	private Operation opcode = Operation.OR;

	public ConditionNode(BoolContext ctx) {
		super(ctx);
	}

	@Override
	public void accept(IASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public <T> T accept(ITypedASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

	/**
	 * Negate the operation.
	 */
	public void negate() {
		switch (opcode) {
		case AND:
			opcode = Operation.OR;
			break;
		case OR:
			opcode = Operation.AND;
			break;
		default:
			break;
		}
	}

	public Operation getOperation() {
		return opcode;
	}
}
