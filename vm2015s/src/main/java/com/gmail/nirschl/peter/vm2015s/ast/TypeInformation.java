/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

/**
 * Provides constants for the type system.
 * 
 * @author pnirschl
 *
 */
public abstract class TypeInformation {

	public static final int NO_TYPE = -1;
	public static final int INT = 0;
	public static final int INT_ARRAY = 1;

	public static String typeToString(int type) {
		if (type <= NO_TYPE) {
			return "<no type>";
		} else if (type == INT) {
			return "integer";
		} else {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < type; i++) {
				sb.append("array of ");
			}
			sb.append(typeToString(INT));
			return sb.toString();
		}
	}
}
