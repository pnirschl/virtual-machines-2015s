/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import java.util.ArrayList;
import java.util.List;

import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.AssignstatContext;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.VardefstatContext;

/**
 * A node that stores some sort of value to a variable or array. This node
 * covers both the variable definition (with included initialization) as well as
 * the regular variable assignment.
 * 
 * @author pnirschl
 *
 */
public class StoreNode extends ASTNode {

	private final boolean definition;
	private final String id;
	private final List<ASTNode> lexpressions = new ArrayList<ASTNode>();
	private int declarationType;

	public StoreNode(VardefstatContext ctx) {
		super(ctx);
		this.definition = true;
		this.id = ctx.vardef().ID().getText();
		this.declarationType = ctx.vardef().type().getChildCount() - 1;
	}

	public StoreNode(AssignstatContext ctx) {
		super(ctx);
		this.definition = false;
		this.id = ctx.lexpr().ID().getText();
		this.declarationType = TypeInformation.NO_TYPE;
	}

	@Override
	public void accept(IASTVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public <T> T accept(ITypedASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

	/**
	 * Indicates if the variable or array being written to is already defined or
	 * not.
	 * 
	 * @return true if this node represents the definition of the variable or
	 *         array.
	 */
	public boolean isDefinition() {
		return definition;
	}

	public void setDeclarationType(int type) {
		this.declarationType = type;
	}

	/**
	 * Get the type that has been declared in the input source.
	 * 
	 * @return the declaration type as integer.
	 */
	public int getDeclarationType() {
		return declarationType;
	}

	/**
	 * Get the expressions that are required to determine the array indices that
	 * shall be accessed. For a regular variable assignment this list is empty.
	 * 
	 * @return a list of {@link com.gmail.nirschl.peter.vm2015s.ast.ASTNode} if
	 *         an array dereferencation should take place or an empty list if
	 *         the node represents a variable assignment.
	 */
	public List<ASTNode> getLExpressions() {
		return lexpressions;
	}

	/**
	 * Get the identifier of the variable or array to be accessed.
	 * 
	 * @return the ID of the variable or array to be accessed.
	 */
	public String getId() {
		return id;
	}
}
