/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

/**
 * A visitor that operates on the elements of the abstract syntax tree (
 * {@link com.gmail.nirschl.peter.vm2015s.ast.AbstractSyntaxTree}).
 * 
 * @author pnirschl
 *
 */
public interface IASTVisitor {
	public void visit(RootNode n);

	public void visit(FunctionNode n);

	public void visit(StatsNode n);

	public void visit(UnsupportedNode n);

	public void visit(ArithmeticNode n);

	public void visit(ConstantNode n);

	public void visit(ReturnNode n);

	public void visit(StoreNode n);

	public void visit(LoadNode n);

	public void visit(ConditionNode n);

	public void visit(ComparisonNode n);

	public void visit(BranchNode n);

	public void visit(WhileLoopNode n);

	public void visit(FunctionCallNode n);

	public void visit(SimpleArrayInitNode n);
}
