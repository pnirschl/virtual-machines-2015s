/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import java.lang.reflect.Method;

import com.gmail.nirschl.peter.vm2015s.codegen.CodeGeneratingASTVisitor;

/**
 * A command that triggers the code generation and executes the input code in
 * the JVM. When executing the object will look for a "main" Method with no
 * arguments and try to invoke it dynamically. An error message will be
 * displayed if no such method exists.
 * 
 * @author pnirschl
 *
 */
public class BytecodeExecutor implements IAppCommand {

	public static final String MAIN_METHOD = "main";
	public static final Class<?>[] NO_PARAMS = {};

	AppParameters parameters = null;

	public void setParameters(AppParameters parameters) {
		this.parameters = parameters;
	}

	public void execute() throws Exception {
		CodeGeneratingASTVisitor visitor = new CodeGeneratingASTVisitor(
				parameters);
		parameters.getAST().getRoot().accept(visitor);

		try {
			AppClassloader classloader = new AppClassloader(parameters,
					visitor.getGeneratedClass());
			Class<?> generatedClass = classloader.loadGeneratedClass();

			Method mainMethod = generatedClass
					.getMethod(MAIN_METHOD, NO_PARAMS);

			Object returnValue = mainMethod.invoke(null, new Object[] {});

			// write byte-code to stdout
			System.out.println("----- Bytecode Execution -----");
			System.out.println("Main function returned: "
					+ returnValue.toString());

		} catch (NoSuchMethodException e) {
			System.out.println("ERROR: No main() function has been defined!");
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
