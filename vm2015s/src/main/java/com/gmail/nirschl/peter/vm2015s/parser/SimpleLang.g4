/**
* Virtual Machines Project (2015S)
* Grammar definition for the parser and the lexer.
* The file is used by ANTLR to generate the parser.
*     
* Copyright (C) 2015 Peter Nirschl.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
grammar SimpleLang;

/* grammar definition */
program: ( funcdef ';' )* ;

funcdef: ID '(' pars ')' stats 'end' ; 

pars: ( ( vardef ',' )* vardef )? ;

vardef: ID ':' type ;

type: ( 'array of' )* 'int' ;

stats: ( stat ';' )* ;

stat: returnstat
    | branchstat
    | loopstat
    | vardefstat
    | arraydefstat
    | assignstat
    | functioncall
    ;

returnstat: 'return' expr;
branchstat: 'if' bool 'then' stats ( 'else' stats )? 'end';
loopstat: 'while' bool 'do' stats 'end'; 
vardefstat: 'var' vardef ':=' expr;
arraydefstat: 'var' vardef ':=' '[' expr ( ',' expr )* ']';
assignstat: lexpr ':=' expr; 

bool: bterm ( 'or' bterm )* ;

bterm: subbool
    | negation
    | lesscomp
    | neqcomp
    ;

subbool: '(' bool ')';
negation: 'not' bterm;
lesscomp: expr '<' expr;
neqcomp: expr '#' expr;

lexpr: ID ( '[' expr ']' )*;

expr: term
    | subterm
    | addterm
    | multerm
    ;

subterm: subterm '-' term	
    | term
    ;

addterm: term '+' addterm
    | term
    ;

multerm: term '*' multerm
    | term
    ;

term: subexpr
    | loadconst
    | loadarray
    | loadvar
    | functioncall
    ;

subexpr: '(' expr ')';
loadconst: NUM;
loadarray: ID ( '[' expr ']' )+;
loadvar: ID;
functioncall: ID '(' ( ( expr ',' )* expr )? ')';


/* lexer definition */
ID: [A-Za-z][A-Za-z0-9]* ;
NUM: DEC_NUM | HEX_NUM ;
DEC_NUM: [0-9]+ ;
HEX_NUM: ('$')[0-9A-Fa-f] ;
WS: [ \t\r\n]+ -> skip ;
COMMENT: '--'(.)*? -> skip ;
