/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser.FuncdefContext;

/**
 * A node that contains the information of a single function definition.
 * Together with its child nodes the whole function can be generated.
 * 
 * @author pnirschl
 *
 */
public class FunctionNode extends ASTNode {

	private String id;
	private SymbolTable parameters = new SymbolTable();

	public FunctionNode(FuncdefContext context) {
		super(context);
		this.id = context.ID().getText();
	}

	public String getId() {
		return id;
	}

	@Override
	public void accept(IASTVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public <T> T accept(ITypedASTVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public SymbolTable getParameters() {
		return parameters;
	}

	public void setParameters(SymbolTable parameters) {
		this.parameters = parameters;
	}
}
