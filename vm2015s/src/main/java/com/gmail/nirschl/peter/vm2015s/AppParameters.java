/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.gmail.nirschl.peter.vm2015s.SimpleLangErrorListener.ErrorEntry;
import com.gmail.nirschl.peter.vm2015s.ast.AbstractSyntaxTree;
import com.gmail.nirschl.peter.vm2015s.ast.SemanticCheckingASTVisitor;
import com.gmail.nirschl.peter.vm2015s.ast.SemanticCheckingASTVisitor.SemanticError;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangLexer;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser;

/**
 * Facade to the Apache Commons CLI library classes. Enables commands (
 * {@link com.gmail.nirschl.peter.vm2015s.IAppCommand}) to receive their
 * parameters and enables the {@link com.gmail.nirschl.peter.vm2015s.App} to
 * execute the commands.
 * 
 * The method closeStreams() must be called after the object is no longer in
 * use.
 * 
 * @author pnirschl
 *
 */
public class AppParameters {

	public static final String CMD_AST = "ast";
	public static final String CMD_HELP = "help";
	public static final String CMD_OUT = "out";
	public static final String CMD_IN = "in";
	public static final String CMD_CODEPRINT = "print";
	public static final String CMD_CLASSNAME = "class";

	private static Options buildOptions() {
		Options options = new Options();
		options.addOption("i", CMD_IN, true,
				"path to the input source file (default: stdin)");
		options.addOption("c", CMD_CLASSNAME, true,
				"name of the class to be generated (Default: GeneratedClass)");
		options.addOption(CMD_AST, false,
				"prints the generated abstract syntax tree (AST) to the standard output");
		options.addOption(CMD_CODEPRINT, false,
				"prints the generated Java byte-code in readable form to the standard output");
		options.addOption("h", CMD_HELP, false, "print this help message");
		return options;
	}

	/**
	 * Print the usage message to the standard output.
	 */
	public static void printUsageMessage() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("vm2015s", buildOptions());
	}

	private CommandLine cmd = null;
	private ArrayList<IAppCommand> commands = new ArrayList<IAppCommand>();
	private List<SemanticError> errors = new ArrayList<SemanticError>();
	private SimpleLangErrorListener errorListener = new SimpleLangErrorListener();
	private ANTLRInputStream inputStream = null;
	private OutputStream outputStream = null;
	private AbstractSyntaxTree ast = null;
	private String classname = "GeneratedClass";
	private boolean helpMode = false;

	/**
	 * Creates a new AppParameters instance by parsing the CLI arguments.
	 * 
	 * @param args
	 *            the CLI arguments passed to the program entry point.
	 */
	public AppParameters(String args[]) throws AppUsageException, IOException {
		try {
			CommandLineParser parser = new org.apache.commons.cli.BasicParser();
			this.cmd = parser.parse(buildOptions(), args);
			buildCommandList();
			if (!helpMode) {
				readParameters();
				setupStreams();
				buildAst();
				if (!hasInputError()) {
					validateAst();
				}
			}
		} catch (ParseException e) {
			throw new AppUsageException(e);
		}
	}

	private void setupStreams() throws IOException {
		String inputFilePath = cmd.getOptionValue(CMD_IN);
		String outputFilePath = cmd.getOptionValue(CMD_OUT);

		if (inputFilePath == null) {
			this.inputStream = new ANTLRInputStream(System.in);
		} else {
			this.inputStream = new ANTLRInputStream(new FileInputStream(
					inputFilePath));
		}

		if (outputFilePath == null) {
			this.outputStream = System.out;
		} else {
			this.outputStream = new FileOutputStream(outputFilePath);
		}
	}

	private void buildCommandList() {
		// help mode enabled?
		if (cmd.hasOption(CMD_HELP)) {
			HelpPrinter help = new HelpPrinter();
			help.setParameters(this);
			commands.add(help);
			helpMode = true;
			return;
		}

		// print AST?
		if (cmd.hasOption(CMD_AST)) {
			ASTPrinter printer = new ASTPrinter();
			printer.setParameters(this);
			commands.add(printer);
		}

		// print byte-code to stdout?
		if (cmd.hasOption(CMD_CODEPRINT)) {
			CodePrinter codePrinter = new CodePrinter();
			codePrinter.setParameters(this);
			commands.add(codePrinter);
		}

		// execute the generated code
		BytecodeExecutor executor = new BytecodeExecutor();
		executor.setParameters(this);
		commands.add(executor);
	}

	private void buildAst() {
		SimpleLangLexer lexer = new SimpleLangLexer(getInputStream());
		lexer.removeErrorListeners();
		lexer.addErrorListener(errorListener);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SimpleLangParser parser = new SimpleLangParser(tokens);
		parser.removeErrorListeners();
		parser.addErrorListener(errorListener);
		if (!errorListener.hasError()) {
			ast = new AbstractSyntaxTree(parser.program());
		}
	}

	private void readParameters() {
		String classname = cmd.getOptionValue(CMD_CLASSNAME);
		if (classname != null) {
			this.classname = classname;
		}
	}

	private void validateAst() {
		SemanticCheckingASTVisitor checkingVisitor = new SemanticCheckingASTVisitor();
		getAST().getRoot().accept(checkingVisitor);
		errors = checkingVisitor.getErrorList();
		if (!checkingVisitor.isSemanticallyCorrect()) {
			// stop further command execution
			commands.clear();
		}
	}

	/**
	 * Closes the output stream if necessary.
	 */
	public void closeStreams() {
		if (outputStream != null) {
			try {
				if (outputStream != System.out) {
					outputStream.close();
				}
			} catch (IOException e) {
				// to be ignored
			}
		}
	}

	/**
	 * Returns whether or not the AST could be generated.
	 * 
	 * @return true if there are neither syntax errors nor semantic errors
	 *         within the input stream, otherwise false.
	 */
	public boolean hasInputError() {
		return errorListener.hasError();
	}

	/**
	 * Print all syntax error messages to the standard output.
	 */
	public void printInputErrors() {
		for (ErrorEntry e : errorListener.getErrors()) {
			System.out.println(e.toString());
		}
	}

	/**
	 * Returns a list of commands to be executed.
	 * 
	 * @return a List of {@link com.gmail.nirschl.peter.vm2015s.IAppCommand} to
	 *         be executed by the current process.
	 */
	public List<IAppCommand> getCommands() {
		return commands;
	}

	/**
	 * Returns an input stream to the source file, which should be parsed.
	 * 
	 * @return an {@link org.antlr.v4.runtime.ANTLRInputStream} to the source
	 *         file.
	 */
	public ANTLRInputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Returns an abstract syntax tree holding the intermediate representation
	 * of the parsed input stream.
	 * 
	 * @return an {@link com.gmail.nirschl.peter.vm2015s.ast.AbstractSyntaxTree}
	 */
	public AbstractSyntaxTree getAST() {
		return ast;
	}

	/**
	 * Returns an output stream to which the .class data including the Java
	 * byte-code should be written.
	 * 
	 * @return an {@link java.io.OutputStream} to the .class output file.
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * Returns the name of the class to be generated.
	 * 
	 * @return a String containing the name of the class to be generated
	 */
	public String getClassName() {
		return classname;
	}

	/**
	 * Get list of semantic errors in the parsed program.
	 * 
	 * @return a List of semantic errors that can be reported to the user.
	 */
	public List<SemanticError> getErrors() {
		return errors;
	}

	public boolean hasSemanticErrors() {
		return !(errors.isEmpty());
	}
}
