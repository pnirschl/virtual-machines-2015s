/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

/**
 * Visitor determining the actual type of a given node (not to be confused with
 * the expected type which is stored in the symbol table).
 * 
 * @author pnirschl
 *
 */
public class TypeDetectingASTVisitor implements ITypedASTVisitor<Integer> {

	public Integer visit(RootNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(FunctionNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(StatsNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(UnsupportedNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(ConditionNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(ComparisonNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(BranchNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(WhileLoopNode n) {
		return TypeInformation.NO_TYPE;
	}

	public Integer visit(FunctionCallNode n) {
		return TypeInformation.INT;
	}

	public Integer visit(ArithmeticNode n) {
		return TypeInformation.INT;
	}

	public Integer visit(ConstantNode n) {
		return TypeInformation.INT;
	}

	public Integer visit(ReturnNode n) {
		// return the type of the child of the node (i.e. the returned type)
		return n.getChildNodes().get(0).accept(this);
	}

	public Integer visit(StoreNode n) {
		// return the type right side of the assignment (i.e. the actual type)
		return n.getChildNodes().get(0).accept(this);
	}

	public Integer visit(LoadNode n) {
		return n.getDeclarationType() - n.getChildNodes().size();
	}

	public Integer visit(SimpleArrayInitNode n) {
		return TypeInformation.INT_ARRAY;
	}
}
