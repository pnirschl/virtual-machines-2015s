/**
 * Virtual Machines Project (2015S)
 * 
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * General syntax error handling facility of the application.
 * 
 * @author pnirschl
 *
 */
public class SimpleLangErrorListener extends BaseErrorListener {

	/**
	 * Stores syntax error information for CLI output.
	 * 
	 * @author pnirschl
	 */
	public static class ErrorEntry {
		private int line;
		private int position;
		private String msg;

		public ErrorEntry(int line, int position, String msg) {
			this.line = line;
			this.position = position;
			this.msg = msg;
		}

		@Override
		public String toString() {
			return "syntax error at line " + line + ":" + position + " - " + msg;
		}
	}

	private static Logger log = LoggerFactory
			.getLogger(SimpleLangErrorListener.class);

	private ArrayList<ErrorEntry> errorList = new ArrayList<ErrorEntry>();

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer,
			Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {

		log.debug("Syntax error at line " + line + ":" + charPositionInLine
				+ " - " + msg);
		errorList.add(new ErrorEntry(line, charPositionInLine, msg));
	}

	/**
	 * Returns whether or not a syntax error was detected.
	 * 
	 * @return true if a syntax error has been detected by the parser, false
	 *         otherwise.
	 */
	public boolean hasError() {
		return (errorList.size() > 0);
	}

	/**
	 * Get the error list.
	 * 
	 * @return a List of error entries, that can be printed to the CLI.
	 */
	public List<ErrorEntry> getErrors() {
		return errorList;
	}

}
