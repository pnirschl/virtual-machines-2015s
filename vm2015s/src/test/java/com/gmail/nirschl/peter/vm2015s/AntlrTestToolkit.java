/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangLexer;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser;

/**
 * Common functions for testing the parser.
 * 
 * @author pnirschl
 */
public class AntlrTestToolkit {

	/**
	 * Create a parser for the given input.
	 * 
	 * @param input
	 *            a program in the target programming language
	 * @return a {@link SimpleLangParser} holding the parsed input
	 */
	public static SimpleLangParser createParser(final String input) {
		return createParser(input, null);
	}

	/**
	 * Create a parser for the given input.
	 * 
	 * @param input
	 *            a program in the target programming language
	 * @param errorListener
	 *            the {@link org.antlr.v4.runtime.ANTLRErrorListener} to be
	 *            added to the lexer and the parser.
	 * @return a {@link SimpleLangParser} holding the parsed input
	 */
	public static SimpleLangParser createParser(final String input,
			ANTLRErrorListener errorListener) {

		ANTLRInputStream stream = new ANTLRInputStream(input);
		SimpleLangLexer lexer = new SimpleLangLexer(stream);
		lexer.removeErrorListeners();
		if (errorListener != null) {
			lexer.addErrorListener(errorListener);
		}
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SimpleLangParser parser = new SimpleLangParser(tokens);
		parser.removeErrorListeners();
		if (errorListener != null) {
			parser.addErrorListener(errorListener);
		}
		return parser;
	}

}
