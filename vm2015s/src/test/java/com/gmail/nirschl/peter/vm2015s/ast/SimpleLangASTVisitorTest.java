/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import static org.junit.Assert.*;

import java.io.IOException;

import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Before;
import org.junit.Test;

import com.gmail.nirschl.peter.vm2015s.AntlrTestToolkit;
import com.gmail.nirschl.peter.vm2015s.parser.SimpleLangParser;

public class SimpleLangASTVisitorTest {

	SimpleLangASTVisitor visitor;

	@Before
	public void setup() {
		this.visitor = new SimpleLangASTVisitor();
	}

	@Test
	public void testEmptyProgram() throws IOException {
		final String program = " ";
		SimpleLangParser parser = AntlrTestToolkit.createParser(program);
		ParseTree tree = parser.program();
		RootNode root = (RootNode) visitor.visit(tree);

		assertEquals(0, root.getChildNodes().size());
	}

	@Test
	public void testSingleFuncdef() throws Exception {
		final String program = "func123() end;";
		SimpleLangParser parser = AntlrTestToolkit.createParser(program);
		ParseTree tree = parser.program();
		RootNode root = (RootNode) visitor.visit(tree);
		assertEquals(1, root.getChildNodes().size());

		FunctionNode funcdef = (FunctionNode) root.getChildNodes().get(0);
		assertEquals("func123", funcdef.getId());
	}

	@Test
	public void testSingleFuncdefWithPars() throws Exception {
		final String program = "foobar(a : int, b : array of int) end;";
		SimpleLangParser parser = AntlrTestToolkit.createParser(program);
		ParseTree tree = parser.program();
		RootNode root = (RootNode) visitor.visit(tree);
		assertEquals(1, root.getChildNodes().size());

		FunctionNode funcdef = (FunctionNode) root.getChildNodes().get(0);
		assertEquals("foobar", funcdef.getId());
	}

	@Test
	public void testTwoFuncdef() throws Exception {
		final String program = "f() end; g23(myparam : int)end;";
		SimpleLangParser parser = AntlrTestToolkit.createParser(program);
		ParseTree tree = parser.program();
		RootNode root = (RootNode) visitor.visit(tree);
		assertEquals(2, root.getChildNodes().size());

		FunctionNode funcdef1 = (FunctionNode) root.getChildNodes().get(0);
		assertEquals("f", funcdef1.getId());

		FunctionNode funcdef2 = (FunctionNode) root.getChildNodes().get(1);
		assertEquals("g23", funcdef2.getId());
	}

}
