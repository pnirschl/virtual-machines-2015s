/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s.ast;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SymbolTableTest {

	private SymbolTable t;

	@Before
	public void setup() {
		t = new SymbolTable();
	}

	@Test(expected = UndefinedVariableUsageException.class)
	public void testGetTypeWithEmptyTable()
			throws UndefinedVariableUsageException {
		t.getVariableType("xyz");
	}

	@Test
	public void testAddInt() throws UndefinedVariableUsageException {
		final int expectedType = 0;
		int actualType;

		t.addVariable("x", expectedType);
		actualType = t.getVariableType("x");
		assertEquals(expectedType, actualType);
	}

	@Test
	public void testAddArray() throws UndefinedVariableUsageException {
		final int expectedType = 1;
		int actualType;

		t.addVariable("myArray", expectedType);
		actualType = t.getVariableType("myArray");
		assertEquals(expectedType, actualType);
	}

	@Test
	public void testAddMultipleDefinitions()
			throws UndefinedVariableUsageException {
		final int expectedType = 3;
		int actualType;

		t.addVariable("a", 0);
		t.addVariable("b", 0);
		t.addVariable("a", 3);
		actualType = t.getVariableType("a");
		assertEquals(expectedType, actualType);
	}

	@Test(expected = UndefinedVariableUsageException.class)
	public void testUndefinedWithNonEmptyTable()
			throws UndefinedVariableUsageException {
		t.addVariable("a", 2);
		t.addVariable("mycoolint", 0);
		t.getVariableType("meh");
	}

	@Test(expected = UndefinedVariableUsageException.class)
	public void testVariableNumberEmpty()
			throws UndefinedVariableUsageException {
		t.getLocalNumber("null");
	}

	@Test(expected = UndefinedVariableUsageException.class)
	public void testVariableNumberUndefined()
			throws UndefinedVariableUsageException {
		t.addVariable("x", 0);
		t.addVariable("example", 0);
		t.getLocalNumber("null");
	}

	@Test
	public void testVariableNumberSingle()
			throws UndefinedVariableUsageException {
		int actual;

		t.addVariable("x", 0);
		actual = t.getLocalNumber("x");
		assertEquals(0, actual);
	}

	@Test
	public void testVariableNumberMulti()
			throws UndefinedVariableUsageException {
		int actual_x;
		int actual_y;
		int actual_z;

		t.addVariable("x", 0);
		t.addVariable("y", 0);
		t.addVariable("z", 0);

		actual_x = t.getLocalNumber("x");
		actual_y = t.getLocalNumber("y");
		actual_z = t.getLocalNumber("z");

		assertNotEquals(actual_x, actual_y);
		assertNotEquals(actual_x, actual_z);
		assertNotEquals(actual_y, actual_z);
	}

	@Test
	public void testDuplicate() throws Exception {
		t.addVariable("abc098", 3);
		t.addVariable("abb12d", 0);

		SymbolTable clone = t.duplicate();
		assertEquals(3, clone.getVariableType("abc098"));
		assertEquals(0, clone.getVariableType("abb12d"));
	}

	@Test(expected = UndefinedVariableUsageException.class)
	public void testDuplicateForDifferentReferences() throws Exception {
		t.addVariable("a", 9);

		SymbolTable clone = t.duplicate();
		assertTrue(clone != t);

		clone.addVariable("b", 3);
		t.getVariableType("b");
	}
}
