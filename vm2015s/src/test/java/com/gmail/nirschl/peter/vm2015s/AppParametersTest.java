/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class for {@link com.gmail.nirschl.peter.vs2015s.AppParameters}.
 * 
 * @author pnirschl
 *
 */
public class AppParametersTest {

	private static Logger log = LoggerFactory
			.getLogger(AppParametersTest.class);
	private static File file = null;
	private AppParameters p = null;

	@BeforeClass
	public static void createTempInputFile() throws IOException {
		file = File.createTempFile("vm2015s-AppParametersTest-", ".txt");
		file.deleteOnExit();
		log.info("Created temporary file: " + file.getAbsolutePath());
	}

	@After
	public void closeStreams() {
		if (p != null) {
			p.closeStreams();
		}
	}

	@Test
	public void testHelp() throws Exception {
		final String args[] = { "--help", "-i", getInputFilePath() };
		p = new AppParameters(args);
		assertEquals(1, p.getCommands().size());
		assertTrue(p.getCommands().get(0) instanceof HelpPrinter);
	}

	@Test
	public void testHelpShort() throws Exception {
		String args[] = { "-h", "-i", getInputFilePath() };
		p = new AppParameters(args);
		assertEquals(1, p.getCommands().size());
		assertTrue(p.getCommands().get(0) instanceof HelpPrinter);
	}

	@Test
	public void testAst() throws Exception {
		String args[] = { "--ast", "-i", getInputFilePath() };
		boolean containsAstPrinter = false;
		p = new AppParameters(args);
		for (IAppCommand cmd : p.getCommands()) {
			if (cmd instanceof ASTPrinter) {
				containsAstPrinter = true;
			}
		}
		assertTrue(containsAstPrinter);
	}

	@Test(expected = AppUsageException.class)
	public void testInvalidArg() throws Exception {
		String args[] = { "--narf", "-i", getInputFilePath() };
		@SuppressWarnings("unused")
		AppParameters p = new AppParameters(args);
	}

	private static final String getInputFilePath() {
		if (file == null) {
			return null;
		}
		return file.getAbsolutePath();
	}

}
