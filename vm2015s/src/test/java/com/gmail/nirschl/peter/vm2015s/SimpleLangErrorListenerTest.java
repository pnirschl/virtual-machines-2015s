/**
 * Virtual Machines Project (2015S)
 *     
 * Copyright (C) 2015 Peter Nirschl.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.gmail.nirschl.peter.vm2015s;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for
 * {@link com.gmail.nirschl.peter.vm2015s.SimpleLangErrorListener}.
 * 
 * @author pnirschl
 *
 */
public class SimpleLangErrorListenerTest {

	SimpleLangErrorListener e;

	@Before
	public void setUp() throws Exception {
		e = new SimpleLangErrorListener();
	}

	@Test
	public void testCorrectEmptyProgram() {
		final String program = "";
		AntlrTestToolkit.createParser(program, e);
		assertEquals(0, e.getErrors().size());
		assertFalse(e.hasError());
	}

	@Test
	public void testUnknownLexeme() {
		final String program = "%foo() end;";
		AntlrTestToolkit.createParser(program, e).program();
		assertEquals(1, e.getErrors().size());
		assertTrue(e.hasError());
	}

	@Test
	public void testSyntaxErrorInStats() {
		final String program = "testfunc() undefined; end;";
		AntlrTestToolkit.createParser(program, e).program();
		assertEquals(1, e.getErrors().size());
		assertTrue(e.hasError());
	}

}
