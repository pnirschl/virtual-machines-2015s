# Virtual Machines (2015S)

Course number at Vienna University of Technology: [185.A49](http://www.complang.tuwien.ac.at/andi/185966) (2015S)

## Abstract

This project is about virtual machines. The goal is to create an interpreter for a simple programming language.
Please find the formal specification of the language at `doc/language-spec.md`.

The interpreter parses given input files and generates Java bytecode, which is injected using a `ClassLoader` and then invoked by the Java Reflection.

## Get started

Compile the project with maven by using the following command:

```
mvn compile
```

You may want to use the Appassembler plugin ( [link](http://mojo.codehaus.org/appassembler/appassembler-maven-plugin/) ). It generates a script that sets up the classpath environment (including all dependencies), so that the application can be started simply by running the script. The following command generates this script:

```
mvn package appassembler:assemble
```

After Appassembler is done, you can start the application by executing the `app` script:

```
./target/appassembler/bin/app
```

It is recommended to start the app with the `-h` or `--help` CLI option the first time you run it. This will print a help message, that lists all CLI options. **As default setting, the application reads from standard input and writes to standard output**. Use the `-i` option to redirect the input to a file.

## About the Author

The author studies Software and Information Engineering at Vienna University of Technology.

## Copyright Notice

Copyright (C) 2015 Peter Nirschl.

The whole work in this repository is released under the terms and conditions of the GNU General Public License version 3.
For further details, refer to [https://www.gnu.org/licenses/gpl-3.0](https://www.gnu.org/licenses/gpl-3.0).
A text version of the license is also distributed and can be accessed at `LICENSE.txt`.