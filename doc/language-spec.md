# Language

This artifact contains a formal specification of the supported programming language.

## Formal Specification

### Parser (Grammar)

The grammar of the programming language being interpreted is defined in the following ANTLR4-styled EBNF:

```
program: ( funcdef ';' )* ;

funcdef: ID '(' pars ')' stats 'end' ; 

pars: ( ( vardef ',' )* vardef )? ;

vardef: ID ':' type ;

type: ( 'array of' )* 'int' ;

stats: ( stat ';' )* ;

stat: returnstat
    | branchstat
    | loopstat
    | vardefstat
    | arraydefstat
    | assignstat
    | functioncall
    ;

returnstat: 'return' expr;
branchstat: 'if' bool 'then' stats ( 'else' stats )? 'end';
loopstat: 'while' bool 'do' stats 'end';
vardefstat: 'var' vardef ':=' expr;
arraydefstat: 'var' vardef ':=' '[' expr ( ',' expr )* ']';
assignstat: lexpr ':=' expr; 

bool: bterm ( 'or' bterm )* ;

bterm: subbool
    | negation
    | lesscomp
    | neqcomp
    ;

subbool: '(' bool ')';
negation: 'not' bterm;
lesscomp: expr '<' expr;
neqcomp: expr '#' expr;

lexpr: ID ( '[' expr ']' )*;

expr: term
    | subterm
    | addterm
    | multerm
    ;

subterm: subterm '-' term	
    | term
    ;

addterm: term '+' addterm
    | term
    ;

multerm: term '*' multerm
    | term
    ;

term: subexpr
    | loadconst
    | loadarray
    | loadvar
    | functioncall
    ;

subexpr: '(' expr ')';
loadconst: NUM;
loadarray: ID ( '[' expr ']' )+;
loadvar: ID;
functioncall: ID '(' ( ( expr ',' )* expr )? ')' ':' type;
```

### Lexer

Numbers can be expressed in decimal and hexadecimal notation. 
The identifiers (`id`) and numeric literals (`num`) are defined by the following regular expressions:

```
ID: [A-Za-z][A-Za-z0-9]* ;
NUM: DEC_NUM | HEX_NUM ;
DEC_NUM: [0-9]+ ;
HEX_NUM: ('$')[0-9A-Fa-f] ;
```

The lexer will recognize identifiers and numeric literals, as well as the following **keywords**:

- `end`
- `array`
- `of`
- `int`
- `return`
- `if`
- `then`
- `else`
- `while`
- `do`
- `var`
- `not`
- `or`

Also the lexer will process the following **symbols**:

- `;`
- `(` and `)`
- `,`
- `:`
- `:=`
- `<`
- `#`
- `[` and `]`
- `-`
- `+`
- `*`

Comments start with the character sequence `--` and end at the end of the line. There is no support for multi-line comments.

All lexemes are **case-sensitive**, i.e. `End` is an identifier, not a keyword.
