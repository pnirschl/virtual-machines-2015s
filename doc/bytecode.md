# Tips & Tricks for Bytecode Handling

This document is a collection of tips & tricks for the development.

## "Disassembling" a .class File

The tool `javap` can be used to see the bytecode of an arbitrary class file in human-readable Assembler-like form.

    javap -c MyClass.class

