# Project Dependencies

## Apache Commons

This project uses the following components of the Apache Commons:

- CLI - Command Line Parser ( [link](http://commons.apache.org/proper/commons-cli/) )
- BCEL - Byte Code Engineering Library ( [link](http://commons.apache.org/proper/commons-bcel/) )

## ANTLR

For generating the parser the ANTLR project (version 4) is being used. ( [link](http://www.antlr.org/) ).
